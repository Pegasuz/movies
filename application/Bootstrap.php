<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{	
	protected function _initIniSettings ()
	{
		ini_set('error_log', APPLICATION_PATH . "/tmp/error" . Date('YmdHi') . ".log");
		ini_set('log_errors_max_len',10000);
		
		Zend_Registry::set('Zend_Locale',new Zend_Locale("nl_BE"));
	}
	
	protected function _initDatabase() {
		$this->bootstrap('Dbs');
		$dbs = $this->getPluginResource('Dbs');

		Zend_Registry::set('db',$dbs->getDbAdapter('movies'));
	}
	
	protected function _initAppAutoload () {
		$autoloader = new Zend_Application_Module_Autoloader(array('namespace' => 'App' , 'basePath' => dirname(__FILE__)));
		$autoloader->addResourceType('webservice','webservices/','Webservice');
	
		return $autoloader;
	}
	
	protected function _initImdb() {
		require_once(APPLICATION_PATH . "/../library/imdb/imdbsearch.class.php");
		require_once(APPLICATION_PATH . "/../library/imdb/imdb.class.php");
		require_once(APPLICATION_PATH . "/../library/imdb/imdb_person.class.php");
	}	
}

