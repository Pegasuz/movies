<?php

class MyController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    	$name = $this->getRequest()->getParam("name",false);
    	
    	$criteria = new Base_Model_Criteria();
    	$criteria->where("login = ?",$name);
    	
    	$userModel = new App_Model_User();
    	$results = $userModel->fetch($criteria);

    	if (count($results) == 1)  {
    		$user = reset($results);
    		
    		if ($user->showPublic()) {
	    		$this->view->user = $user;
	    		$criteria = new Base_Model_Criteria();
	    		$criteria->where("userID = ?",$user->getUserID());
	    		$criteria->sort("title");
	    		
	    		$movieModel = new App_Model_Movie();
	    		$results = $movieModel->fetch($criteria);
	    		
	    		$this->view->movies = $results;
	    		
	    		$this->view->numTotal = count($results);
	    		 
	    		// Get num tv shows
	    		$criteria = new Base_Model_Criteria();
	    		$criteria->where("userID = ?",$user->getUserID());
	    		$criteria->where("type=?",App_Model_Movie::TYPE_TVSHOW);
	    		 
	    		$this->view->numShows = $movieModel->fetchCount($criteria);
	    		$resultShows = $movieModel->fetch($criteria);
	    		 
	    		$numSeasons = 0;
	    		foreach ($resultShows as $show) {
	    			$s = explode(",",$show->getSeasons());
	    			$numSeasons += count($s);
	    		}
	    		 
	    		$this->view->numMovies = ($this->view->numTotal - $this->view->numShows);
	    		$this->view->numAll = ($this->view->numMovies + $numSeasons);
	    		$this->view->numSeasons = $numSeasons;
    		} else {
    			$this->_redirect("/");
    		}
    	} else {
    		$this->_redirect("/");
    	}
    	
    	
    }
    
	public function __call($methodName, $args) {
		$cleanMethodName = str_replace('Action','',$methodName);
		$this->_forward("index","my",null,array("name"=>$cleanMethodName));
	}


}

