<?php

class PersonController extends Zend_Controller_Action
{
	protected $userID = 1;
	
    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }
	
    public function getUserID() {
    	return $this->userID;
    }
    
    public function showAction() {
    	$id = (string)$this->getRequest()->getParam("id");
    	$person = new imdb_person($id);
    	if ($person) {
    		$this->view->person = $person;
    		
    		// Get all our movies
    		$criteria = new Base_Model_Criteria();
    		$criteria->where("userID = ?",$this->getUserID());
    		
    		$movies = new App_Model_Movie();
    		$results = $movies->fetch($criteria);
    		$this->view->mymovies = $results;
    	}
    }
    
    public function getimageAction() {
    	$url = $this->getRequest()->getParam("url");
    	$id = $this->getRequest()->getParam("id");
    	$thumb = $this->getRequest()->getParam("thumb");
    	if ($thumb != "true") {
    		$thumb = false;
    	} else {
    		$thumb = true;
    	}
    	
    	if ($url) {
	    	return $this->serveImage($url,$thumb);
    	} elseif ($id) {
    		$movie = new imdb_person($id);
    		if($movie) {
    			return $this->serveImage($movie->photo(),$thumb);
    		}
    	}
    	exit;
    }
    
    private function serveImage($url,$thumb=false) {
    	$imgFolder = APPLICATION_PATH . "/tmp/img/person/";
    	$path = $imgFolder . urlencode($url);
    	
    	if (file_exists($path)) {
    		$file = file_get_contents($path);
    	} else {
    		$file = file_get_contents($url);
    		// Cache image
    		file_put_contents($path, $file);
    		$file = file_get_contents($path);
    	}
		if ($thumb && $file) {
			$file = $this->resizeImage($path, 50,100);
			
		}
		$cache_seconds = 2592000; // 30 days
		
		header("Cache-Control: max-age=$cache_seconds");
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $cache_seconds) . ' GMT');
		header('Pragma: cache');
		header('Content-Type: image/jpg');
		//header('Content-length: ' . filesize($image_to_stream));
		
    	echo $file;
    	exit;
    }
    
    private function resizeImage($file,$w,$h,$crop=false) {
    	$newFileName = $file . "_" . $w . "_" . $h;
    	
    	if (!file_exists($newFileName)) {
    		list($width, $height) = getimagesize($file);
    		$r = $width / $height;
    		if ($crop) {
    			if ($width > $height) {
    				$width = ceil($width-($width*($r-$w/$h)));
    			} else {
    				$height = ceil($height-($height*($r-$w/$h)));
    			}
    			$newwidth = $w;
    			$newheight = $h;
    		} else {
    			if ($w/$h > $r) {
    				$newwidth = $h*$r;
    				$newheight = $h;
    			} else {
    				$newheight = $w/$r;
    				$newwidth = $w;
    			}
    		}
    		$src = @imagecreatefromjpeg($file);
    		$dst = imagecreatetruecolor($newwidth, $newheight);
    		imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
    		imagejpeg($dst,$newFileName);
    	}    	
    	return file_get_contents($newFileName);
    }

}

