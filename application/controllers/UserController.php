<?php

class UserController extends Zend_Controller_Action
{
	
	protected $user = false;
	
    public function init()
    {
        /* Initialize action controller here */
    	
    	$zs = new Zend_Session_Namespace("user");
    	if ($zs->userID) {	
    		$userModel = new App_Model_User();
    		$user = $userModel->find($zs->userID);
    		
    		if ($user) {
    			$this->user = $user;
    		} else {
    			$this->_forward("login");
    		}
    		
    	} else {
    		$this->_forward("login");
    	}
    	
    }
    
    protected function getUser() {
    	return $this->user;
    }

    public function indexAction()
    {
     	$this->_forward("index","movie");
    }
	
    
    public function loginAction() {
    	$form = new Zend_Form();
    	$login = new Zend_Form_Element_Text("login");
    	$login->setLabel("Username");
    	$login->setRequired(true);
    	
    	$pw = new Zend_Form_Element_Password("pw");
    	$pw->setLabel("Password");
    	$pw->setRequired(true);
    	
    	$btn = new Zend_Form_Element_Submit("Login");
    	$btn->setLabel("Grant me access");
    	
    	$form->addElements(array($login,$pw,$btn));
    	
    	if ($this->getRequest()->isPost()) {
    		if ($form->isValid($this->getRequest()->getParams())) {
    			if ($this->authenticate($form->getValues())) {
    				$this->_redirect("/");
    			} else {
    				$el = $form->getElement('login');
    				$el->addError("No valid user/pw!");	
    			}
    		}
    	}
    	$this->view->form = $form;
    	
    }
    
    protected function authenticate($formData) {
    	$isValidUser = false;
    	
    	$criteria = new Base_Model_Criteria();
    	$criteria->where("login = ?",$formData["login"]);
    	$criteria->where("pw = ?",md5($formData["pw"]));
    	
    	$userModel = new App_Model_User();
    	$result = $userModel->fetch($criteria);
    	
    	if (count($result) == 1) {
    		// Authenticate
    		$isValidUser = true;
    		$user = reset($result);
    		
    		$zs = new Zend_Session_Namespace("user");
    		$zs->userID = $user->getUserID();
    		$zs->login = $user->getLogin();
    		$zs->admin = $user->isAdmin();
    	}
    	
    	return $isValidUser;
    }
    
    public function logoutAction() {
    	$zs = new Zend_Session_Namespace("user");
    	$zs->unsetAll();
    	
    	$this->_redirect("/");
    }
    
    public function profileAction() {
    	
    	$user = $this->getUser();
    	 
    	$this->view->user = $user;
    	 
    	$form = new Zend_Form("profile");
    	    	
    	$email = new Zend_Form_Element_Text("email");
    	$email->setLabel("E-mail");
    	$email->setRequired(true);
    	$email->setValue($user->getEmail());
    	
    	$pw1 = new Zend_Form_Element_Password("pw1");
    	$pw1->setLabel("Password");
    	
    	$pw2 = new Zend_Form_Element_Password("pw2");
    	$pw2->setLabel("Password (2x)");
    	
    	$public = new Zend_Form_Element_Checkbox("showPublic");
    	$public->setLabel("Show collection public");
    	$public->setValue($user->showPublic());
    	
    	
    	$submit = new Zend_Form_Element_Submit("submit");
    	$submit->setLabel("Edit user");
    	
    	$form->addElements(array($email,$pw1,$pw2,$public,$submit));
    	
    	
    	if ($this->getRequest()->isPost()) {
    		if ($form->isValid($this->getRequest()->getParams())) {
    			$options = $form->getValues();
    			if ($options["pw1"] == $options["pw2"] && $options["pw1"]!= "") {
    				$options["pw"] = md5($options["pw1"]);
    			}
    			 
    			 
    			$user->setOptions($options);
    			$user->save();
    			$this->_redirect("/user/profile");
    		}
    	}
    	
    	$this->view->form = $form;
    }

}

