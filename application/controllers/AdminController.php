<?php

class AdminController extends Zend_Controller_Action
{

	protected $user = false;
	
    public function init()
    {
        /* Initialize action controller here */
    	
    	$zs = new Zend_Session_Namespace("user");
    	if ($zs->userID) {	
    		$userModel = new App_Model_User();
    		$user = $userModel->find($zs->userID);
    		
    		if ($user) {
    			$this->user = $user;
    			if (!$user->isAdmin()) {
    				$this->_forward("login");
    			}
    		} else {
    			$this->_forward("login");
    		}
    		
    	} else {
    		$this->_forward("login");
    	}
    	
    }
    
    protected function getUser() {
    	return $this->user;
    }

    public function indexAction()
    {
        $criteria = new Base_Model_Criteria();
        
        $userModel = new App_Model_User();
        
        
        $this->view->users = $userModel->fetch($criteria);
    }
    
    public function addUserAction() {
    	$form = new Zend_Form("adduser");
    	
    	$login = new Zend_Form_Element_Text("login");
    	$login->setLabel("Username");
    	$login->setRequired(true);
    	
    	$email = new Zend_Form_Element_Text("email");
    	$email->setLabel("E-mail");
    	$email->setRequired(true);
    	
    	$admin = new Zend_Form_Element_Checkbox("isAdmin");
    	$admin->setLabel("Administrator?");
    	
    	$public = new Zend_Form_Element_Checkbox("showPublic");
    	$public->setLabel("Show public");
    	
    	$pw1 = new Zend_Form_Element_Password("pw1");
    	$pw1->setLabel("Password");
    	$pw1->setRequired(true);
    	
    	$pw2 = new Zend_Form_Element_Password("pw2");
    	$pw2->setLabel("Password (2x)");
    	$pw2->setRequired(true);
    	
    	$submit = new Zend_Form_Element_Submit("submit");
    	$submit->setLabel("Add new user");
    	
    	$form->addElements(array($login,$email,$pw1,$pw2,$admin,$public,$submit));

    	if ($this->getRequest()->isPost()) {
    		if ($form->isValid($this->getRequest()->getParams())) {
    			$options = $form->getValues();
    			if ($options["pw1"] == $options["pw2"]) {
    				$options["pw"] = md5($options["pw1"]);
	    			
	    			$user = new App_Model_User();
	    			$user->setOptions($options);
	    			$user->save();
	
	    			$this->_redirect("/admin");
	    		}
    		} 
    	}
    	
    	$this->view->form = $form;
    }
	
    public function editUserAction() {
    	$id = $this->getRequest()->getParam("id");
    	$criteria = new Base_Model_Criteria();
    	$criteria->where("userID = ?",$id);
    	
    	$userModel = new App_Model_User();
    	$result = $userModel->fetch($criteria);
    	$user = reset($result);
    	
    	$this->view->user = $user;
    	
    	$form = new Zend_Form("adduser");
    	 
    	$login = new Zend_Form_Element_Text("login");
    	$login->setLabel("Username");
    	$login->setRequired(true);
    	$login->setValue($user->getLogin());
    	 
    	$email = new Zend_Form_Element_Text("email");
    	$email->setLabel("E-mail");
    	$email->setRequired(true);
    	$email->setValue($user->getEmail());
    	 
    	$admin = new Zend_Form_Element_Checkbox("isAdmin");
    	$admin->setLabel("Administrator?");
    	$admin->setValue($user->isAdmin());
    	
    	$public = new Zend_Form_Element_Checkbox("showPublic");
    	$public->setLabel("Show collection public");
    	$public->setValue($user->showPublic());
    	 
    	$pw1 = new Zend_Form_Element_Password("pw1");
    	$pw1->setLabel("Password");
    	 
    	$pw2 = new Zend_Form_Element_Password("pw2");
    	$pw2->setLabel("Password (2x)");
    	
    	
    	 
    	$submit = new Zend_Form_Element_Submit("submit");
    	$submit->setLabel("Edit user");
    	 
    	$form->addElements(array($login,$email,$pw1,$pw2,$admin,$public,$submit));
    
    
    	if ($this->getRequest()->isPost()) {
    		if ($form->isValid($this->getRequest()->getParams())) {
    			$options = $form->getValues();
    			if ($options["pw1"] == $options["pw2"] && $options["pw1"]!= "") {
    				$options["pw"] = md5($options["pw1"]);
    			}
    			
    			
    			$user->setOptions($options);
    			$user->save();
    			$this->_redirect("/admin");
    		}
    	}
    	 
    	$this->view->form = $form;
    }
    
    public function deleteUserAction() {
    	$id = $this->getRequest()->getParam("id");
    	$criteria = new Base_Model_Criteria();
    	$criteria->where("userID = ?",$id);
    	 
    	$userModel = new App_Model_User();
    	$result = $userModel->fetch($criteria);
    	$user = reset($result);
    	
    	$user->delete();
    	
    	// Delete all movies of this user
    	$criteria = new Base_Model_Criteria();
    	$criteria->where("userId = ?",$user->getUserID());
    	
    	$movieModel = new App_Model_DataMapper_Movie();
    	
    	$results = $movieModel->fetch($criteria);
    	if ($results) {
	    	foreach ($results as $movie) {
	    		$movie->delete();
	    	}
    	}
    	$this->_redirect("/admin");
    }

}

