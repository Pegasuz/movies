<?php

class MovieController extends Zend_Controller_Action
{

    protected $user = false;
    
	public function init()
    {
    	$zs = new Zend_Session_Namespace("user");
    	if ($zs->userID) {	
    		$userModel = new App_Model_User();
    		$user = $userModel->find($zs->userID);
    		
    		if ($user) {
    			$this->user = $user;
    			$this->view->user = $user;
    		} else {
    			$this->_redirect("/user/login");
    		}
    		
    	} else {
    		$this->_redirect("/user/login");
    	}
    }
    
    public function getUser() {
    	return $this->user;
    }

    public function indexAction()
    {    	
    	$criteria = new Base_Model_Criteria();
    	$criteria->where("userID = ?",$this->user->getUserID());
    	$criteria->sort("title");
    	
    	$filter = $this->getRequest()->getParam("filter",false);
    	if ($filter) {
    		switch($filter) {
    			case "movies":
    				$criteria->where("type = ?",App_Model_Movie::TYPE_MOVIE);
    				break;
    			case "tvshows":
    				$criteria->where("type = ?",App_Model_Movie::TYPE_TVSHOW);
    				break;
    			case "rating":
    				$minRating = $this->getRequest()->getParam("minrating",0);
    				$this->view->minRating = $minRating;
    				$criteria->where("rating >= ?",$minRating);
    				break;
    		}
    		$this->view->filter = $filter;
    	}		
    	
    	$myMovies = new App_Model_Movie();
    	$results = $myMovies->fetch($criteria);
    			
    	$this->view->movies = $results;
    	
    	$this->view->numTotal = count($results);
    	
    	
    	
    	// Get num tv shows
    	$criteria = new Base_Model_Criteria();
    	$criteria->where("userID = ?",$this->user->getUserID());
    	$criteria->where("type=?",App_Model_Movie::TYPE_TVSHOW);
    	
    	$this->view->numShows = $myMovies->fetchCount($criteria);
    	$resultShows = $myMovies->fetch($criteria);
    	
    	$numSeasons = 0;
    	foreach ($resultShows as $show) {
    		$s = explode(",",$show->getSeasons());
    		$numSeasons += count($s);
    	}
    	
    	$this->view->numMovies = ($this->view->numTotal - $this->view->numShows);
    	$this->view->numAll = ($this->view->numMovies + $numSeasons);
    	$this->view->numSeasons = $numSeasons;
    }
	
    public function showAction() {
    	$this->view->headScript()->appendFile('/js/movie.show.js');
    	$id = (string)$this->getRequest()->getParam("id");
    	if ($id) {
    		$movie = new imdb($id);
    		if($movie) {
    			$this->view->movie = $movie;
    			
    			$criteria = new Base_Model_Criteria();
    			$criteria->where("imdbID = ?",$movie->imdbId());
    			
    			$myMovies = new App_Model_Movie();
    			$results = $myMovies->fetch($criteria);
    			
    			$result = reset($results);
    			if ($result instanceof  App_Model_Movie) {
    				$this->view->myMovie = $result;
    				
    			}
    		}
    	}
    	
    }
    
    public function searchAction() {
    	$q = $this->getRequest()->getParam("q");
    	if ($q) {
	    	$search = new imdbsearch();
	    	$search->setsearchname($q);
	    	$results = $search->results();
	    	
	    	$this->view->movies = $results;
	    	$this->view->q = $q;
	    	
	    	// Get all our movies
	    	$criteria = new Base_Model_Criteria();
	    	$criteria->where("userID = ?",$this->getUser()->getUserID());
	    	
	    	$movies = new App_Model_Movie();
	    	$results = $movies->fetch($criteria);
	    	$this->view->mymovies = $results;
    	}
    }
    
    public function addAction() {
    	$id = (string)$this->getRequest()->getParam("id");
    	$movie = new imdb($id);
    	if($movie) {
    		$date = new DateTime();
    		switch($movie->movietype()) {
    			case "TV Series":
    				$type = App_Model_Movie::TYPE_TVSHOW;
    				break;
    			case "Movie":
    			default:
    				$type = App_Model_Movie::TYPE_MOVIE;
    				break;
    		}
    		$plot = reset($movie->plot());
    		$data = array(
    			"userID" => $this->user->getUserID(),
    			"imdbID" => $movie->imdbid(),
    			"added" => $date->format("c"),
    			"title" => $movie->title(),
    			"description" => $plot,
    			"img" => $movie->photo(),
    			"type" => $type,
    			"rating" => $movie->rating(),
    			"seasons" => ""
    		);
    		
    		$myMovie = new App_Model_Movie();
    		$myMovie->setOptions($data);
    		$myMovie->save();
    		
    		$this->_redirect('/movie/show/id/' . $movie->imdbid());
    	}
    	
    }
    
    public function removeAction() {
    	$id = $this->getRequest()->getParam("id");
    	$myMovie = new App_Model_Movie();
    	$myMovie->find($id);
    	$imdb = $myMovie->getImdbID();
    	$myMovie->delete();
    	
    	$this->_redirect('/movie/show/id/' . $imdb);
    }
    
    public function updateAction() {
    	$id = $this->getRequest()->getParam("id");
    	$season = $this->getRequest()->getParam("season");
    	$status = $this->getRequest()->getParam("status");
    	
    	$myMovie = new App_Model_Movie();
    	$myMovie->find($id);
    	
    	$seasons = explode(",",$myMovie->getSeasons());
    	switch($status) {
    		case "add":
    			$seasons[] = $season;
    			break;
    		case "remove":
    			foreach ($seasons as $s) {
    				if ($s != $season) {
    					$newSeasons[] = $s;
    				}
    			}
    			$seasons = $newSeasons;
    			break;
    	}
    	
    	$myMovie->setSeasons(implode(",",$seasons));
    	$myMovie->save();
    	$this->_helper->json(array("success"=>true));    		
		exit;
    }
    
    public function getimageAction() {
    	$url = $this->getRequest()->getParam("url");
    	$id = $this->getRequest()->getParam("id");
    	
    	if ($url) {
	    	return $this->serveImage($url);
    	} elseif ($id) {
    		$movie = new imdb($id);
    		if($movie) {
    			return $this->serveImage($movie->photo());
    		}
    	}
    }
    
    private function serveImage($url) {
    	$imgFolder = APPLICATION_PATH . "/tmp/img/title/";
    
    	if (file_exists($imgFolder . urlencode($url))) {
    		$file = file_get_contents($imgFolder . urlencode($url));
    	} else {
    		$file = file_get_contents($url);
    		// Cache image
    		file_put_contents($imgFolder . urlencode($url), $file);
    	}
    	$cache_seconds = 2592000; // 30 days
    	
    	header("Cache-Control: max-age=$cache_seconds");
    	header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $cache_seconds) . ' GMT');
    	header('Pragma: cache');
    	header('Content-Type: image/jpg');
    	echo $file;
    	exit;
    }
    
 
}

