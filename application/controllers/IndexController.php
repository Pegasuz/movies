<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    	
    	$zs = new Zend_Session_Namespace("user");
    	if ($zs->userID) {
    		$userModel = new App_Model_User();
    		$user = $userModel->find($zs->userID);
    	
    		if ($user) {
    			$this->user = $user;
    		} else {
    			$this->_redirect("/user/login");
    		}
    	
    	} else {
    		$this->_redirect("/user/login");
    	}
    }

    public function indexAction()
    {
        // action body
    	$this->_redirect("/movie");
    	
    	
    	$criteria = new Base_Model_Criteria();
    	$criteria->where("userID = ?",1);
    	
    	$user = new App_Model_User();
    	$result = $user->fetch($criteria);
    	$result = reset($result);
    	
    	$q = $this->getRequest()->getParam("q");
    	
    	$search = new imdbsearch();
    	$search->setsearchname($q);
    	$results = $search->results();
    	
    	foreach($results as $result) {
    		$cache = new App_Model_MovieCache();
    		$cache->fillFromImdb($result);
//     		echo $result->title();
//     		echo " - ";
//     		echo $result->imdbid();
//     		//echo " - ";
//     		//echo "<img src='" . $result->photo(true) . "' />";
//     		echo "<br>";
    		
    		//echo serialize($cache);
    		$cache->save();
    		exit;
    	}
    }


}

