<?php

class SoapController extends Zend_Controller_Action
{
	//change this to your WSDL URI!
	protected $_WSDL_URI;
	

    public function indexAction()
    {
    	$this->_helper->viewRenderer->setNoRender();
  		$this->_helper->layout->disableLayout();
		
		if(isset($_GET['wsdl'])) {
			//return the WSDL
			$this->handleWSDL();
		} else {
			//handle SOAP request
			$this->handleSOAP();
		}
    }
    
    private function handleWSDL ()
    {
    	$autodiscover = new Zend_Soap_AutoDiscover();
    	$autodiscover->setClass('App_Service_Soap_Movie');
    	$autodiscover->handle();
    }
    
    private function handleSOAP ()
    {
    	$soap = new Zend_Soap_Server($this->_WSDL_URI);
    	$soap->setClass('App_Service_Soap_Movie');
    	$soap->handle();
    }
	
    
    public function __construct (Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
    	$this->_WSDL_URI = 'http://'.$_SERVER['HTTP_HOST'].'/soap?wsdl';
    	parent::__construct($request, $response, $invokeArgs);
    }

}

