<?php
class App_Model_User extends Base_Model_ModelAbstract {
    protected $_dataMapperClass = 'App_Model_DataMapper_User';

    protected $_identifiers = array('userID');

    protected $_fields = array(
            'userID',
            'email',
            'login',
            'pw',
    		'isAdmin',
    		'showPublic'
    );

    protected $_userID = null;

    protected $_email = null;

    protected $_login = null;

    protected $_pw = null;
    
    protected $_isAdmin = null;
    
    protected $_showPublic = null;
    
    public function isAdmin() {
    	if ($this->_isAdmin == 1) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    public function showPublic() {
    	if ($this->_showPublic == 1) {
    		return true;
    	} else {
    		return false;
    	}
    }

}