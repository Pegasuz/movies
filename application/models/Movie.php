<?php
class App_Model_Movie extends Base_Model_ModelAbstract {
    protected $_dataMapperClass = 'App_Model_DataMapper_Movie';

    protected $_identifiers = array('id');

    protected $_fields = array(
            'id',
    		'userID',
            'imdbID',
            'added',
            'title',
    		'description',
    		'img',
    		'type',
    		'rating',
    		'seasons'
    );

    protected $_id = null;
    
    protected $_userID = null;

    protected $_imdbID = null;

    protected $_added = null;

    protected $_title = null;
    
    protected $_description = null;
    
    protected $_img = null;
    
    protected $_type = null;
    
    protected $_rating = 0;
    
    protected $_seasons = null;
    
    const TYPE_MOVIE = "movie";
    const TYPE_TVSHOW = "tvshow";

}