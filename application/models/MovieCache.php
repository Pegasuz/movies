<?php
class App_Model_MovieCache extends Base_Model_ModelAbstract {
    protected $_dataMapperClass = 'App_Model_DataMapper_MovieCache';

    protected $_identifiers = array('imdbID');

    protected $_fields = array(
            'imdbID',
            'moviedata',
    );

    protected $_imdbID = null;

    protected $_moviedata = null;
	
    protected $_title = null;
    
    
    public function fillFromImdb($data) {
    	$this->title = $data->title();
    	$this->setImdbID($data->imdbId());
    }
    
    
    public function save() {
    	$this->setMoviedata(serialize($this));
    	parent::save();
    }
}